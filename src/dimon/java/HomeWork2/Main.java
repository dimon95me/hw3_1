package dimon.java.HomeWork2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner in = new Scanner(System.in);
        System.out.println("Enter strings count:");
        int n = in.nextInt();
        String[] strings = new String[n];

        for(int i=0; i<n; i++){
            System.out.println("Enter new string:");
            strings[i]=in.next();
        }

        int maxLength = strings[0].length();
        int maxString = 0;
        for(int i=0; i<n; i++) {
            if(maxLength<strings[i].length()){
                maxString = i;
            }
        }
        System.out.println("Maximum string:\n"+strings[maxString]+" "+strings[maxString].length());
    }


}
